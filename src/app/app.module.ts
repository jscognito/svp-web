import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ViewLibModule } from 'view-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ViewLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
